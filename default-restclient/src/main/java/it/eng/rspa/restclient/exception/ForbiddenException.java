package it.eng.rspa.restclient.exception;

public class ForbiddenException extends RuntimeException {
	private static final long serialVersionUID = 2148364608343881555L;

	public ForbiddenException(String message){
		super(message);
	}
}
