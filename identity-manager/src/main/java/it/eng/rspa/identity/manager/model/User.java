package it.eng.rspa.identity.manager.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class User {

	private String id;
	private String username;
	private String email;
	private Optional<List<Role>> roles;
	// Organization - Map1
	// where Map1 is Role type "organizationRole,membershipType" - List of roles
	private Optional<Map<Organization,Map<String,List<Role>>>> organizationsRoles;
	
	public User() {}
	
	public User(String username, String email) {
		this(username,username,email);
	}

	public User(String id, String username, String email) {
		this.id = id;
		this.username = username;
		this.email = email;
		this.roles = Optional.of(new ArrayList<Role>());
		this.organizationsRoles = Optional.of(new HashMap<Organization,Map<String,List<Role>>>());
	}

	public User(String id, String username, String email, List<Role> roles,
			Map<Organization,Map<String,List<Role>>> organizationsRoles) {
		this.id = id;
		this.username = username;
		this.email = email;
		this.roles = Optional.ofNullable(roles);
		this.organizationsRoles =  Optional.ofNullable(organizationsRoles);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Optional<List<Role>> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = Optional.ofNullable(roles);
	}

	public Optional<Map<Organization,Map<String,List<Role>>>> getOrganizationsRoles() {
		return organizationsRoles;
	}

	public void setOrganizationsRoles(Map<Organization,Map<String,List<Role>>> organizationsRoles) {
		this.organizationsRoles = Optional.ofNullable(organizationsRoles);
	}
	
	
}
