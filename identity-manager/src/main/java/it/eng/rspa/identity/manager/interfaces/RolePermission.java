package it.eng.rspa.identity.manager.interfaces;

import it.eng.rspa.identity.manager.model.Permission;

public interface RolePermission {
	Permission toPermission();
}
