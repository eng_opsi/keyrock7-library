package it.eng.rspa.identity.manager.interfaces;

import it.eng.rspa.identity.manager.model.Token;
import it.eng.rspa.identity.manager.model.User;

public interface OAuth2Provider {
	public String getAuthorizationCode();
	public Token getAccessToken(String code, String client_id, String client_secret, String redirectUri);
	public Token refreshToken(String refresh_token, String client_id, String client_secret);
	public User getUserInfo(String token);
}
