package it.eng.rspa.identity.manager.interfaces;

import it.eng.rspa.identity.manager.model.User;

public interface AuthenticatedUser {
	User toUser();
}
