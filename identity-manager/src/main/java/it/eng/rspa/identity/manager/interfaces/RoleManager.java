package it.eng.rspa.identity.manager.interfaces;

public interface RoleManager {
//	public Set<Role> getUserRoles(String userId);
//	public Set<Organization> getUserOrganizations(String userId);
//	public Set<Role> getUserRoleByOrganizationId(String userId, String organizationId);
	public String getUserOrganizationRole(String userId,String organizationId);
}
