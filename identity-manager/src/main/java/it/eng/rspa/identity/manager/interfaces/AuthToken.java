package it.eng.rspa.identity.manager.interfaces;

import it.eng.rspa.identity.manager.model.Token;

public interface AuthToken {
	Token toToken();
}
