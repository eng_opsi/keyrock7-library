package it.eng.rspa.identity.manager.interfaces;

import it.eng.rspa.identity.manager.model.Role;

public interface UserRole {
	Role toRole();
}
