package it.eng.rspa.identity.manager.model;

import java.util.Optional;

public class Token {
	private String token;
	private Optional<String> expiresAt;
	private Optional<String> refreshToken;

	public Token() {
	}

	public Token(String token) {
		this(token, "", "");
	}

	public Token(String token, String expiresAt) {
		this(token, expiresAt, "");
	}

	public Token(String token, String expiresAt, String refreshToken) {
		this.token = token;
		this.expiresAt = Optional.ofNullable(expiresAt);
		this.refreshToken = Optional.ofNullable(refreshToken);
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Optional<String> getExpiresAt() {
		return expiresAt;
	}

	public void setExpiresAt(Optional<String> expiresAt) {
		this.expiresAt = expiresAt;
	}

	public Optional<String> getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(Optional<String> refreshToken) {
		this.refreshToken = refreshToken;
	}
}
