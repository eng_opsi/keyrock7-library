package it.eng.rspa.identity.manager.interfaces;

import it.eng.rspa.identity.manager.model.Organization;

public interface UserOrganization {
	Organization toOrganization();
}
