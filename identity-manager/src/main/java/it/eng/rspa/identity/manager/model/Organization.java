package it.eng.rspa.identity.manager.model;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public class Organization {
	private String id;
	private String name;
	private Optional<String> location;
	private Optional<String> description;
	private Optional<String> website;
	//Map containing member id - Map1
	//Map1 Contains User Serialization - Role List
	private Optional<Map<String,Map<User,List<Role>>>> members;
	
	public Organization() {}
	
	public Organization(String id,String name) {
		this(id,name,null,null,null,null);
	}
	
	public Organization(String id,String name,String location) {
		this(id,name,location,null,null,null);
	}
	
	public Organization(String id,String name,String location,String description,String website) {
		this(id,name,location,description,website,null);
	}
	
	public Organization(String id,String name, String location, String description,
			String website,Map<String,Map<User,List<Role>>> members) {
		this.id = id;
		this.name = name;
		this.location = Optional.ofNullable(location);
		this.description = Optional.ofNullable(description);
		this.website = Optional.ofNullable(website);
		this.members = Optional.ofNullable(members);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Optional<String> getLocation() {
		return location;
	}

	public void setLocation(Optional<String> location) {
		this.location = location;
	}

	public Optional<String> getDescription() {
		return description;
	}

	public void setDescription(Optional<String> description) {
		this.description = description;
	}

	public Optional<String> getWebsite() {
		return website;
	}

	public void setWebsite(Optional<String> website) {
		this.website = website;
	}

	public Optional<Map<String, Map<User, List<Role>>>> getMembers() {
		return members;
	}

	public void setMembers(Optional<Map<String, Map<User, List<Role>>>> members) {
		this.members = members;
	}
	
}
