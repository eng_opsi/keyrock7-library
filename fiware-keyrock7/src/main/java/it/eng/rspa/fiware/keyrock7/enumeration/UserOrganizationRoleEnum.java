package it.eng.rspa.fiware.keyrock7.enumeration;

public enum UserOrganizationRoleEnum {
	owner("owner"),
	member("member");
	
	public String value;
    UserOrganizationRoleEnum(String value) 
    { 
        this.value = value; 
    } 
}
