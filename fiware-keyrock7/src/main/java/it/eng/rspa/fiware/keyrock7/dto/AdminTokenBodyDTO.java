package it.eng.rspa.fiware.keyrock7.dto;

import it.eng.rspa.identity.manager.model.Credentials;

public class AdminTokenBodyDTO {
	private String name;
	private String password;
	
	public AdminTokenBodyDTO() {}
	
	public AdminTokenBodyDTO(Credentials adminCredentials) {
		this.setName(adminCredentials.getUsername());
		this.setPassword(adminCredentials.getPassword());
	}
	
	public AdminTokenBodyDTO(String name, String password) {
		super();
		this.name = name;
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	
	
}
