package it.eng.rspa.fiware.keyrock7.dto;

import java.util.List;

public class UserOrganizationDTO extends OrganizationDTO{
		private List<RoleDTO> roles;
		
		public List<RoleDTO> getRoles() {
			return this.roles;
		}
		
		public void setRoles(List<RoleDTO> roles) {
			this.roles = roles;
		}
}
