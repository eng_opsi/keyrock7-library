package it.eng.rspa.fiware.keyrock7.dto;

public class OrganizationUserDTO {
	private String user_id;
	private String organization_id;
	private String role;
	
	public OrganizationUserDTO() {}

	public OrganizationUserDTO(String user_id, String organization_id, String role) {
		super();
		this.user_id = user_id;
		this.organization_id = organization_id;
		this.role = role;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getOrganization_id() {
		return organization_id;
	}

	public void setOrganization_id(String organization_id) {
		this.organization_id = organization_id;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	
}
