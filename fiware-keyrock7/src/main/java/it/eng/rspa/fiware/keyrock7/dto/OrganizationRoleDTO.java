package it.eng.rspa.fiware.keyrock7.dto;

public class OrganizationRoleDTO {
	private OrganizationUserDTO organization_user;
	
	public OrganizationRoleDTO() {}

	public OrganizationRoleDTO(OrganizationUserDTO organization_user) {
		this.organization_user = organization_user;
	}

	public OrganizationUserDTO getOrganization_user() {
		return organization_user;
	}

	public void setOrganization_user(OrganizationUserDTO organization_user) {
		this.organization_user = organization_user;
	}
	
	
}
