package it.eng.rspa.fiware.keyrock7.dto;

import java.util.HashSet;

import it.eng.rspa.identity.manager.interfaces.UserRole;
import it.eng.rspa.identity.manager.model.Permission;
import it.eng.rspa.identity.manager.model.Role;

public class RoleDTO implements UserRole {

	private String id;
	private String name;
	
	
	public RoleDTO() {}
	
	public RoleDTO(String id,String name) {
		this.id = id;
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public Role toRole() {		
		return new Role(this.id,this.name,new HashSet<Permission>());
	}
		
}
