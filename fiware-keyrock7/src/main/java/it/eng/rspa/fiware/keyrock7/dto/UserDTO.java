package it.eng.rspa.fiware.keyrock7.dto;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.eng.rspa.identity.manager.interfaces.AuthenticatedUser;
import it.eng.rspa.identity.manager.model.Organization;
import it.eng.rspa.identity.manager.model.Role;
import it.eng.rspa.identity.manager.model.User;


public class UserDTO implements AuthenticatedUser {
	private String id;
	private String username;
	private String email;
	private String app_azf_domain;
	private String authorization_decision;
	private Boolean isGravatarEnabled;
	private String app_id;
	private List<RoleDTO> roles;
	private String displayName;
	private List<UserOrganizationDTO> organizations;
	private List<ApplicationDTO> trusted_apps;
	
	public static final String ORGANIZATION_ROLE = "organizationRole";
	public static final String MEMBERSHIP_TYPE = "membershipType";
	
	
	public UserDTO() {}

	public UserDTO(String app_azf_domain, String authorization_decision, Boolean isGravatarEnabled, String app_id,
			List<RoleDTO> roles, String displayName, List<UserOrganizationDTO> organizations) {
		this.app_azf_domain = app_azf_domain;
		this.authorization_decision = authorization_decision;
		this.isGravatarEnabled = isGravatarEnabled;
		this.app_id = app_id;
		this.roles = roles;
		this.displayName = displayName;
		this.organizations = organizations;
		this.trusted_apps = new ArrayList<ApplicationDTO>();
	}

	public String getApp_azf_domain() {
		return app_azf_domain;
	}

	public void setApp_azf_domain(String app_azf_domain) {
		this.app_azf_domain = app_azf_domain;
	}

	public String getAuthorization_decision() {
		return authorization_decision;
	}

	public void setAuthorization_decision(String authorization_decision) {
		this.authorization_decision = authorization_decision;
	}

	public Boolean getIsGravatarEnabled() {
		return isGravatarEnabled;
	}

	public void setIsGravatarEnabled(Boolean isGravatarEnabled) {
		this.isGravatarEnabled = isGravatarEnabled;
	}

	public String getApp_id() {
		return app_id;
	}

	public void setApp_id(String app_id) {
		this.app_id = app_id;
	}

	public List<RoleDTO> getRoles() {
		return roles;
	}

	public void setRoles(List<RoleDTO> roles) {
		this.roles = roles;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public List<UserOrganizationDTO> getOrganizations() {
		return organizations;
	}

	public void setOrganizations(List<UserOrganizationDTO> organizations) {
		this.organizations = organizations;
	}

	@Override
	public User toUser() {
		List<Role> roles= new ArrayList<Role>();
		for(RoleDTO r: this.roles) {
			roles.add(r.toRole());
		}
		
		Map<Organization,Map<String,List<Role>>> uOrgs= new HashMap<Organization,Map<String,List<Role>>>();
		for(UserOrganizationDTO o: this.organizations) {
			Map<String,List<Role>> rMap = new HashMap<String,List<Role>>();
			List<Role> rList = new ArrayList<Role>();
			for(RoleDTO r : o.getRoles()) {
				rList.add(r.toRole());
			}
			rMap.put(UserDTO.ORGANIZATION_ROLE, rList);
			uOrgs.put(o.toOrganization(), rMap);
		}
		return new User(this.id,this.username,this.email,roles,uOrgs);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<ApplicationDTO> getTrustedApplication() {
		return trusted_apps;
	}

	public void setTrustedApplication(List<ApplicationDTO> trustedApplication) {
		this.trusted_apps = trustedApplication;
	}
	
}
