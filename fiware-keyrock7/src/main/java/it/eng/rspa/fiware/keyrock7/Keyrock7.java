package it.eng.rspa.fiware.keyrock7;

import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import it.eng.rspa.fiware.keyrock7.dto.AccessTokenDTO;
import it.eng.rspa.fiware.keyrock7.dto.AdminTokenBodyDTO;
import it.eng.rspa.fiware.keyrock7.dto.OrganizationRoleDTO;
import it.eng.rspa.fiware.keyrock7.dto.UserDTO;
import it.eng.rspa.identity.manager.model.Organization;
import it.eng.rspa.identity.manager.model.Role;
import it.eng.rspa.identity.manager.model.Token;
import it.eng.rspa.identity.manager.model.User;
import it.eng.rspa.idm.fiware.IdentityManager;
import it.eng.rspa.restclient.HTTPResponse;

public class Keyrock7 extends IdentityManager {

	private static final Logger LOGGER = Logger.getLogger(Keyrock7.class.getName());

	private static String path_user = "/user";
	private static String path_token = "/oauth2/token";
	private static String path_admin_token = "/v1/auth/tokens";

	public Keyrock7() {
	}

	@Override
	public String getAuthorizationCode() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Token getAccessToken(String code, String client_id, String client_secret, String redirectUri) {
		String url = getBaseUrl() + path_token;
		String auth = "Basic " + new String(Base64.getEncoder().encode((client_id + ":" + client_secret).getBytes()));

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", MediaType.APPLICATION_FORM_URLENCODED);
		headers.put("Authorization", auth);

		String reqData = "grant_type=authorization_code" + "&code=" + code + "&redirect_uri=" + redirectUri;

		String responseData;
		AccessTokenDTO out;

		try {
			responseData = restClient.consumePost(url, reqData, headers).getBody();
			out = jsonSerde.toEntity(responseData, AccessTokenDTO.class);

		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Cannot get Access Token");
			out = null;
		}

		return out.toToken();
	}

	@Override
	public Token refreshToken(String refresh_token, String client_id, String client_secret) {
		String url = getBaseUrl() + path_token;

		String contentType = MediaType.APPLICATION_FORM_URLENCODED;
		String Authorization = "Basic "
				+ new String(Base64.getEncoder().encode((client_id + ":" + client_secret).getBytes()));

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", contentType);
		headers.put("Authorization", Authorization);

		String reqData = "grant_type=refresh_token" + "&client_id=" + client_id + "&client_secret=" + client_secret
				+ "&refresh_token=" + refresh_token;

		AccessTokenDTO out;
		try {
			String responseData = restClient.consumePost(url, reqData, headers).getBody();
			out = jsonSerde.toEntity(responseData, AccessTokenDTO.class);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Cannot get Access Token");
			out = null;
		}
		return out.toToken();
	}

	@Override
	public User getUserInfo(String token) {
		UserDTO out = null;
		try {
			String url = getBaseUrl() + path_user + "?access_token=" + token;
			String responseData = restClient.consumeGet(url, new HashMap<String, String>()).getBody();
			out = jsonSerde.toEntity(responseData, UserDTO.class);
		} catch (Exception e) {
			e.printStackTrace();
			
		}
		
		User user = out.toUser();
		
		addOrganizationMembershipRoles(user);
		
		return user;
	}

	
	private void addOrganizationMembershipRoles(User user) {
		Map<Organization,Map<String,List<Role>>> oRolesMap = user.getOrganizationsRoles().orElse(null);
		if(oRolesMap != null) {
			Set<Organization> userOrganizations = oRolesMap.keySet();
			for(Organization o : userOrganizations) {
				Map<String,List<Role>> oMap = oRolesMap.get(o);
				String membershipRole = getUserOrganizationRole(user.getId(), o.getId());
				Role mRole = new Role(o.getId().concat("_").concat(UserDTO.MEMBERSHIP_TYPE),membershipRole);
				oMap.put(UserDTO.MEMBERSHIP_TYPE,Arrays.asList(mRole));
			}
		}
	}

	public String getUserOrganizationRole(String userId,String organizationId) {
		Token adminToken = this.getAdminToken();
		
		HashMap<String, String> headers = new HashMap<String, String>();
								headers.put("X-Auth-token", adminToken.getToken());
								
		String idmEndpoint = Keyrock7.getBaseUrl() 
								+ "/v1/organizations/"
								+ organizationId
								+ "/users/"
								+ userId
								+ "/organization_roles";
		

		OrganizationRoleDTO oRole = null;
		try { 
			HTTPResponse resp = restClient.consumeGet(idmEndpoint, headers);
			oRole = jsonSerde.toEntity(resp.getBody(), OrganizationRoleDTO.class);
			if (resp.getResponseCode() != 200 && resp.getResponseCode() != 201 && resp.getResponseCode() != 301) {
				throw new Exception(String.valueOf(resp.getResponseCode()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		String orgRoleString = oRole.getOrganization_user().getRole();
		return orgRoleString;
	}

	@Override
	public Token getAdminToken() {
		Set<String> methods = new HashSet<String>();
		methods.add("password");

		HashMap<String, String> headers = new HashMap<String, String>();
		String adminToken = null;
		String idmEndpoint = Keyrock7.getBaseUrl() + Keyrock7.path_admin_token;

		AdminTokenBodyDTO adminTokenDTO = new AdminTokenBodyDTO(getAdminCredentials());
		String json = jsonSerde.toJson(adminTokenDTO);
		try {
			HTTPResponse resp = restClient.consumePost(idmEndpoint, json, headers);
			if (resp.getResponseCode() != 200 && resp.getResponseCode() != 201 && resp.getResponseCode() != 301) {
				throw new Exception(String.valueOf(resp.getResponseCode()));
			}

			MultivaluedMap<String, String> respHeaders = resp.getHeaders();
			adminToken = respHeaders.getFirst("X-Subject-Token");
		} catch (Exception e) {
			e.printStackTrace();
		}

		Token out = new Token(adminToken);
		return out;
	}

}
