package it.eng.rspa.fiware.keyrock7.dto;

import java.util.List;
import it.eng.rspa.identity.manager.interfaces.AuthToken;
import it.eng.rspa.identity.manager.model.Token;

public class AccessTokenDTO implements AuthToken {
	
	private String access_token;
	private String token_type;
	private Integer expires_in;
	private String refresh_token;
	private List<String> scope;
	private String state;
	
	public AccessTokenDTO() {}
	
	public AccessTokenDTO(String access_token, String token_type,
			Integer expires_in, String refresh_token, List<String> scope, String state) {
	
		this.access_token = access_token;
		this.token_type = token_type;
		this.expires_in = expires_in;
		this.refresh_token = refresh_token;
		this.scope = scope;
		this.state = state;
	}

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public String getToken_type() {
		return token_type;
	}

	public void setToken_type(String token_type) {
		this.token_type = token_type;
	}

	public Integer getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(Integer expires_in) {
		this.expires_in = expires_in;
	}

	public String getRefresh_token() {
		return refresh_token;
	}

	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}

	public List<String> getScope() {
		return scope;
	}

	public void setScope(List<String> scope) {
		this.scope = scope;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public Token toToken() {
		return new Token(this.access_token,Integer.toString(this.expires_in),this.refresh_token);
	}
}
