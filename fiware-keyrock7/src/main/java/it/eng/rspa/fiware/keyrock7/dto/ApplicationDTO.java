package it.eng.rspa.fiware.keyrock7.dto;

public class ApplicationDTO {
	private String id;
	private String name;
	private String description;
	private String secret;
	private String url;
	private String redirect_uri;
	private String image;
	private String grant_type;
	private String response_type;
	private String token_types;
	private String jwt_secret;
	private String client_type;
	private String scope;
	private String extra;
	
	public ApplicationDTO() {}

	public ApplicationDTO(String id, String name, String description, String secret, String url, String redirect_uri,
			String image, String grant_type, String response_type, String token_types, String jwt_secret,
			String client_type, String scope, String extra) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.secret = secret;
		this.url = url;
		this.redirect_uri = redirect_uri;
		this.image = image;
		this.grant_type = grant_type;
		this.response_type = response_type;
		this.token_types = token_types;
		this.jwt_secret = jwt_secret;
		this.client_type = client_type;
		this.scope = scope;
		this.extra = extra;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getRedirect_uri() {
		return redirect_uri;
	}

	public void setRedirect_uri(String redirect_uri) {
		this.redirect_uri = redirect_uri;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getGrant_type() {
		return grant_type;
	}

	public void setGrant_type(String grant_type) {
		this.grant_type = grant_type;
	}

	public String getResponse_type() {
		return response_type;
	}

	public void setResponse_type(String response_type) {
		this.response_type = response_type;
	}

	public String getToken_types() {
		return token_types;
	}

	public void setToken_types(String token_types) {
		this.token_types = token_types;
	}

	public String getJwt_secret() {
		return jwt_secret;
	}

	public void setJwt_secret(String jwt_secret) {
		this.jwt_secret = jwt_secret;
	}

	public String getClient_type() {
		return client_type;
	}

	public void setClient_type(String client_type) {
		this.client_type = client_type;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}
	
	
}
