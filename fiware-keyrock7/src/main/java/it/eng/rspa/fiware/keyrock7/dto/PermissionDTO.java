package it.eng.rspa.fiware.keyrock7.dto;

import java.util.Optional;

import it.eng.rspa.identity.manager.interfaces.RolePermission;
import it.eng.rspa.identity.manager.model.Permission;

public class PermissionDTO implements RolePermission{
	private String id;
	private String name;
	private Optional<String> description;
	private Optional<Boolean> is_internal;
	private Optional<String> action;
	private Optional<String> resource;
	private Optional<String> xml;
	private Optional<String> oauth_client_id;
	
	
	public PermissionDTO() {
	}

	public PermissionDTO(String id, String name, Optional<String> description, Optional<Boolean> is_internal,
			Optional<String> action, Optional<String> resource, Optional<String> xml,
			Optional<String> oauth_client_id) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.is_internal = is_internal;
		this.action = action;
		this.resource = resource;
		this.xml = xml;
		this.oauth_client_id = oauth_client_id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Optional<String> getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = Optional.ofNullable(description);
	}

	public Optional<Boolean> getIs_internal() {
		return is_internal;
	}

	public void setIs_internal(Boolean is_internal) {
		this.is_internal = Optional.ofNullable(is_internal);
	}

	public Optional<String> getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = Optional.ofNullable(action);
	}

	public Optional<String> getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = Optional.ofNullable(resource);
	}

	public Optional<String> getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = Optional.ofNullable(xml);
	}

	public Optional<String> getOauth_client_id() {
		return oauth_client_id;
	}

	public void setOauth_client_id(String oauth_client_id) {
		this.oauth_client_id = Optional.ofNullable(oauth_client_id);
	}

	@Override
	public Permission toPermission() {
		return new Permission(this.id,this.name);
	}
	
}
