package it.eng.rspa.fiware.keyrock7.dto;

import java.util.Optional;

import it.eng.rspa.identity.manager.interfaces.UserOrganization;
import it.eng.rspa.identity.manager.model.Organization;

public class OrganizationDTO implements UserOrganization {
	
	private String id;
	private String name;
	private Optional<String> description;
	private Optional<String> website;
	private Optional<String> image;
	
	public OrganizationDTO() {}
	
	public OrganizationDTO(String id,String name) {
		this(id,name,null,null,null);
	}
	
	public OrganizationDTO(String id,String name,String description,String website, String image) {
		this.id = id;
		this.name = name;
		this.description = Optional.ofNullable(description);
		this.website = Optional.ofNullable(website);
		this.image = Optional.ofNullable(image);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Optional<String> getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = Optional.ofNullable(description);
	}

	public Optional<String> getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = Optional.ofNullable(website);
	}

	public Optional<String> getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = Optional.ofNullable(image);
	}

	@Override
	public Organization toOrganization() {
		return new Organization(this.id,this.name,null,this.description.orElse(null),this.website.orElse(null),null);
	}

}
