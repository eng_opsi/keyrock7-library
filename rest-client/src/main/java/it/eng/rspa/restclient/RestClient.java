package it.eng.rspa.restclient;

import java.util.Map;


public interface RestClient {
	public HTTPResponse consumePost(String url, Object body, Map<String, String> headers) throws Exception;
	public HTTPResponse consumeGet(String url, Map<String, String> headers) throws Exception;
	public HTTPResponse consumeDelete(String url, Map<String, String> headers) throws Exception;
	public HTTPResponse consumePut (String url, Object body, String type, Map<String, String> headers) throws Exception;
	public HTTPResponse consumePatch(String url, Object body, Map<String, String> headers) throws Exception;
}
