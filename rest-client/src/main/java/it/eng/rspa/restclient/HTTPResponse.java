package it.eng.rspa.restclient;

import javax.ws.rs.core.MultivaluedMap;

public class HTTPResponse {
	public MultivaluedMap<String,String> headers;
	public Integer responseCode;
	public String body;
	
	public HTTPResponse() {}
	
	public HTTPResponse(MultivaluedMap<String, String> headers, Integer responseCode, String body) {
		super();
		this.headers = headers;
		this.responseCode = responseCode;
		this.body = body;
	}

	public MultivaluedMap<String, String> getHeaders() {
		return headers;
	}

	public void setHeaders(MultivaluedMap<String, String> headers) {
		this.headers = headers;
	}

	public Integer getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

}
